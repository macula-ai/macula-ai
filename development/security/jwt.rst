What is JWT?
============

JWT stands for a JSON web token. It is basically part of the modern authorization and authentication frameworks such as OAuth and OpenID. It helps to securely transfer information between the client and server by validating and encrypting the structure of information being sent.

In our product as well we have used JWT for authorization purposes to help make our system much more secure. Since JWTs can be signed, a server can verify the authenticity of the session data and trust that it hasn’t been tamped with. JWTs can also be encrypted to obscure the session data which prevents users from reading or modifying it. Thus reducing the risk of security vulnerabilites that could emerge in the system.
    
When a user logins with there username and password we authenticate them by making sure that there username and pasword is correct but the authorization part is done by the use of JWT by actually making sure that the user that sends the request to our server is the same user that actually logged in during the authentication process. It's about authorizing this user to have access to the particular server it sends request for.
    
The JSON web token is represented as shown in the image below.  The left hand side shows the encoded web token which is sent from the client to the server as a string that consists of three components. Each component is delimited by a :code:`.` (period) character.
    
The right hand side of the image shows the three decoded components consisting of the Header, Payload and Signature.
    
* A header is majorly used to describe the cryptographic algorithm applied to the JWT for signing and encryption.
    
* The payload consists of all the information that is stored in the token which in our case is all the required information about the user for authentication purposes.
    
* The payload and header is then cryptographically signed with RSA256 where the signature is used to ensure that the information in the token was not tampered and to verify the sender of the JWT.  The three components are then :code:`Base64` encoded and concatenated together with periods delimiter, which results in the encoded token on left hand side.

.. image:: Schema/jwt.jpeg
        :align: center
