How JWT works?
==============

From the previous two parts we understood about what is JWT and how traditional user login system works. In this part we would focus on describing how JWT works differently from the traditional user system.

In JWT, instead of storing the information on the server inside the session memory what happens instead is the server creates a JSON web token and actually encodes and serializes that and signs it with its own secret key so that the server knows if anyone tampered with it then it is invalid based on the signature that it did. This JSON web token is then sent back to the browser.

The main difference here from traditional system is that nothing is being stored on the server. The JWT has all the information about the user built into it. The client can choose to store the information however it wants which means we can use the same JSON web token across multiple servers that we run without getting into the problems of one server has certain session and the other doesn't. Thus, it is extremely useful when we have multiple servers as it is a stateless authentication mechanism as the user state is never saved in the server memory. The server's routes will check for valid JWT from the client and if present the user will be allowed access to the protected resources.

The below protocol flow represents how JWT works between the client and server. The resource server is the API server we are trying to access information on since it holds the protected resource and the authorization server is the server where the user request is approved or denied since it verifies the identity of users.

.. image:: Schema/jwt_client.jpeg
        :align: center
    
    
