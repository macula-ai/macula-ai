How traditional user login system works?
========================================

Traditional user login system uses session ID and cookies. The way it is normally done is by using a session. For example, you have a session ID that you send down in the cookies of the browser and then everytime the client makes the request they send that session ID to the server and the sever checks in its memory storage as to which user corresponds to that session ID, finds that user and then it does the authorization to make sure the user has access. The below protocol flow explains the traditional system in more detail:

    .. image:: Schema/Traditional.jpeg
            :align: center

    1. At first, the user logins in through the client by using the POST API end point of user login by mentioning there username and password.
    
    2. As soon as the request gets to the server, the server does the authentication to make sure the user is valid or not. Incase the user is authenticated, the user is stored inside the session which is stored in the memory of the server and they get a unique ID which corresponds to that part in the memory.

    3. This unquie ID which is the session ID is then sent back to the browser using a cookie so that the browser always has this session ID that it sends to the server everytime it sends the request.
     
     
The above three steps explains the flow of traditional user login system for an inital request. However, when the client needs to make another request for example they want to go to a new page in the application, the session ID gets sent along with the cookie that it corresponds to and is then sent to the server. At the server, it goes into the session memory and it validates if there is something in memory that corresponds to that particular session ID which came in request if so then it knows which user sent the request and checks if the user is authorized to have futher resources. If the user is authorized, it sends back the successful response to the client after verification.
    
    
    
    
    
