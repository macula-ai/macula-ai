Released Model
============================================


Model Details and Specifications
----------------------------------

The model used for DIS is using Siamese Networks to perform One-Shot learning. This is a method to handle defect detection as an Anomaly Detection, it does not learn to classify but to differentiate. 

Input to the model is a pair of images and output is a similarity score, however, the designed framework works in a way that it gives class labels at the end as a result. 




Model DataSet Requirements
---------------------------
Prior to deploying a model on-site in production, a model needs to be pre-trained so that it can be used for the inference of captured images in real-time. In order to train a model to perform an inspection of parts/objects, a dataset containing two types of images are required: images with defects and images without defects. A minimum number of images is a function of how complex the structure of an object is and the complexity of the defect itself. For each defect type and different object, a dataset with a minimum number of images is required in order to train a model. 