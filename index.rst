

.. image:: https://readthedocs.org/projects/sphinx-rtd-theme/badge/?version=latest
  :target: http://sphinx-rtd-theme.readthedocs.io/en/latest/?badge=latest
  :alt: Documentation Status
.. image:: https://img.shields.io/badge/Maintainer-Dataperformers-green
   :alt: Maintainer
   



.. toctree::
   :maxdepth: 2
   :caption:  Macula Module
   :name: macula-module

   module/macula_module
   module/architecture
   module/user_guide


.. toctree::
   :maxdepth: 1
   :caption: Released model
   :hidden:
   :name: sec-rele-model

   released_models

.. toctree::
   :maxdepth: 2
   :caption: Developer Guide
   :hidden:
   :name: development-guide

   development/api/index
   development/api/v1
   development/security/index

.. toctree::
   :maxdepth: 1
   :caption: Version
   :name: sec-ver

   version
   
   



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
