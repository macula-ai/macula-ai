Overall Solution Description
=============================

The Macula backend is an application server which manages the stored images captured by multiple cameras,
and performs inference requests towards the NVIDIA TensorRT Inference Server. Inspection Backend and
Inspection Dashboard run on docker containers, as part of a Kubernetes cluster. Deployment on Kubernetes allows
for easy scaling of the solution. Below diagram demonstrates the high level call flow of image processing:

.. image:: ../img/macula-architecture.png

The above diagram displays the main Macula Backend which polls a shared NAS directory for new images
that are saved from a live camera feed in real-time. The camera, in turn, sends images for inference to the NVIDIA TensorRT
Inference Server (NTRT-IS). As can be seen from diagram 1, the inference can be done on premises for images that are
captured on a remote site. The IB can send images for inference one by one or in batches, the Inference Server (NTRT-IS)
then uses the appropriate model to perform inference on an image(s) and sends back a response to IB with the model
output. Inference results, image(s), and other statistical information is then displayed on the main Macula Dashboard.