Version
=============================

# Release Version

Make sure all PRs under milestone `v1.0.1` are closed, then close the milestone.
Using below command to generate release notes.

Release notes
---------------------------

v1.0.1