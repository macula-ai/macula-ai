API v1
======

The Macula API uses :abbr:`REST (Representational State Transfer)`.
JSON is returned by all API responses including errors
and HTTP response status codes are to designate success and failure.

.. contents:: Table of contents
   :local:
   :backlinks: none
   :depth: 3


Authentication and authorization
--------------------------------

Requests to the Macula public API are for public and private information.
All endpoints require authentication.


Token
~~~~~

The ``Authorization`` HTTP header can be specified with ``Token <your-access-token>``
to authenticate as a user and have the same permissions that the user itself.


Session
~~~~~~~

Session authentication is allowed on very specific endpoints,
to allow hitting the API when reading documentation.

When a user is trying to authenticate via session,
:abbr:`CSRF (Cross-site request forgery)` check is performed.

.. note:: In this section, we will use macula-admin as a username and macula123! as a password. Make sure to have your credentials.


Resources
---------

This section shows all the resources that are currently available in APIv1.
There are some URL attributes that applies to all of these resources:

:?fields=:

   Specify which fields are going to be returned in the response.




Inference Manager
~~~~~~~~~~~~~~~~~~

Login
+++++++++++++

.. http:get:: /v1/auth/login

    Retrieve the token from the authentification service

    **Example request**:

    .. tabs::

        .. code-tab:: bash

            $ curl \
              -X POST http://macula-ai.com/v1/auth/login
              -H "Content-Type: application/json"
              -d  '{"username":"macula-admin","password":"macula123!"}'

        .. code-tab:: python

            import requests
            URL = 'http://macula-ai.com/v1/auth/login'
            data = json.load(open('body.json', 'rb'))
            response = requests.post(URL, json=data)
            print(response.json())

    **Example response**:

    .. sourcecode:: json

        {
            "data": {
                "auth_token": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2MDMyMTQ5MDcsIm5iZiI6MTYwMzIxNDkwNywianRpIjoiNjEyMTY0MGUtZGU0MC00MGJkLWFlNzAtYTdmNDRmZmI5Nzg3IiwiZXhwIjoxNjAzMjE4NTA3LCJzdWIiOiJtaWN0b3JzaCIsImZyZXNoIjpmYWxzZSwidHlwZSI6ImFjY2VzcyIsInVzZXJfY2xhaW1zIjp7InJvbGVzIjpbIkFETUlOIl19fQ.FRoLrS1eukU6zxYW6CiIpi01fIthrmUP8ha7Kc1wq88",
                "roles": [
                    "ADMIN"
                ]
            }
        }


AI Agent create
++++++++++++++++

.. http:put:: /v1/ai-agent

    Import a project under authenticated user.

    **Example request**:

    .. tabs::

        .. code-tab:: bash

            $ curl \
              -X POST \
              -H "Authorization: Token <token>" http://macula-ai/v1/ai-agent

        .. code-tab:: python

            import requests
            import json
            URL = 'http://macula-ai/v1/ai-agent'
            TOKEN = '<token>'
            HEADERS = {'Authorization': f'token {TOKEN}'}
            response = requests.post(
                URL,
                headers=HEADERS,
            )
            print(response.json())

    The content of ``body.json`` is like,

    .. sourcecode:: json

        {
            "data": {
                "agent_id": 49,
                "batch_size": 64,
                "callbacks": [
                    "weightcheckpoint",
                    " confusionmatrix",
                    " jsonlogger"
                ],
                "camera_id": null,
                "data_json": "./data/dataset/Semiconductor_v0.0.0_128X128_1000_images.json",
                "description": null,
                "epoch": 10,
                "gpu": [
                    -1
                ],
                "images_data_dir": "./data/dataset/image",
                "img_ext": "jpg",
                "inference_path": null,
                "init_epoch": 0,
                "input_shape": [
                    128,
                    128,
                    3
                ],
                "last_layer_activation": "sigmoid",
                "learning_rate": 0.0001,
                "log_dir": "./data/logs",
                "loss_list": [
                    "bce"
                ],
                "metrics": [
                    "accuracy ",
                    "precision",
                    " recall",
                    " confusionmatrix"
                ],
                "model_id": 1,
                "model_path": "./data/models",
                "model_type": "classification",
                "normalize_confusion_matrix": false,
                "optimizer_id": "Adam",
                "pretrained": true,
                "random_brightness": false,
                "random_crop": true,
                "random_flip": true,
                "random_rotate": true,
                "runtime": 1577854800.0,
                "save_best_model": "true",
                "split_index": [
                    0.7,
                    0.3
                ],
                "train_verbose": 1,
                "training_id": 49,
                "updated_on": "2020-01-01T00:00:00Z",
                "weight_dir": "./data/weights"
            }
        }

    **Example response**:

    `See Project details <#project-details>`__


AI Agent listing
++++++++++++++++

.. http:get:: /v1/ai-agent

    Retrieve a list of all versions for a project.

    **Example request**:

    .. tabs::

        .. code-tab:: bash

            $ curl -H "Authorization: Token <token>" http://macula-ai/v1/ai-agent

        .. code-tab:: python

            import requests
            URL = 'http://macula-ai/v1/ai-agent'
            TOKEN = '<token>'
            HEADERS = {'Authorization': f'token {TOKEN}'}
            response = requests.get(URL, headers=HEADERS)
            print(response.json())

    **Example response**:

    .. sourcecode:: json

        {
            "items": [
                {
                    "agent_id": 1,
                    "batch_size": 64,
                    "callbacks": [
                        "weightcheckpoint",
                        " confusionmatrix",
                        " jsonlogger"
                    ],
                    "camera_id": null,
                    "data_json": "./data/dataset/Friday_Semiconductor_v0.0.0/Semiconductor_v0.0.0_128X128_1000_images.json",
                    "description": null,
                    "epoch": 10,
                    "gpu": [
                        -1
                    ],
                    "images_data_dir": "./data/dataset/Friday_Semiconductor_v0.0.0/image",
                    "img_ext": "jpg",
                    "inference_path": null,
                    "init_epoch": 0,
                    "input_shape": [
                        128,
                        128,
                        3
                    ],
                    "last_layer_activation": "sigmoid",
                    "learning_rate": 0.0001,
                    "log_dir": "./data/logs",
                    "loss_list": [
                        "bce"
                    ],
                    "metrics": [
                        "accuracy ",
                        "precision",
                        " recall",
                        " confusionmatrix"
                    ],
                    "model_id": 1,
                    "model_path": "/home/yoann",
                    "model_type": "classification",
                    "normalize_confusion_matrix": false,
                    "optimizer_id": "Adam",
                    "pretrained": true,
                    "random_brightness": false,
                    "random_crop": true,
                    "random_flip": true,
                    "random_rotate": true,
                    "runtime": 1577854800.0,
                    "save_best_model": "true",
                    "split_index": [
                        0.7,
                        0.3
                    ],
                    "train_verbose": 1,
                    "training_id": 1,
                    "updated_on": "2020-01-01T00:00:00Z",
                    "weight_dir": "./data/weights"
                }
            ],
            "pagination": {
                "count": 1,
                "limit": 50,
                "offset": 0,
                "total_count": 1
            }
        }


AI Agent detail
++++++++++++++++

.. http:get:: /v1/ai-agent/(integer:ai_agent)

    Retrieve details of a single version.

    **Example request**:

    .. tabs::

        .. code-tab:: bash

            $ curl -H "Authorization: Token <token>" http://macula-ai/v1/ai-agent/(integer:ai_agent)

        .. code-tab:: python

            import requests
            URL = 'http://macula-ai/v1/ai-agent/(integer:ai_agent)'
            TOKEN = '<token>'
            HEADERS = {'Authorization': f'token {TOKEN}'}
            response = requests.get(URL, headers=HEADERS)
            print(response.json())

    **Example response**:

    .. sourcecode:: json

        {
            "data": {
                "agent_id": 49,
                "batch_size": 64,
                "callbacks": [
                    "weightcheckpoint",
                    " confusionmatrix",
                    " jsonlogger"
                ],
                "camera_id": null,
                "data_json": "./data/dataset/Semiconductor_v0.0.0_128X128_1000_images.json",
                "description": null,
                "epoch": 10,
                "gpu": [
                    -1
                ],
                "images_data_dir": "./data/dataset/image",
                "img_ext": "jpg",
                "inference_path": null,
                "init_epoch": 0,
                "input_shape": [
                    128,
                    128,
                    3
                ],
                "last_layer_activation": "sigmoid",
                "learning_rate": 0.0001,
                "log_dir": "./data/logs",
                "loss_list": [
                    "bce"
                ],
                "metrics": [
                    "accuracy ",
                    "precision",
                    " recall",
                    " confusionmatrix"
                ],
                "model_id": 1,
                "model_path": "./data/models",
                "model_type": "classification",
                "normalize_confusion_matrix": false,
                "optimizer_id": "Adam",
                "pretrained": true,
                "random_brightness": false,
                "random_crop": true,
                "random_flip": true,
                "random_rotate": true,
                "runtime": 1577854800.0,
                "save_best_model": "true",
                "split_index": [
                    0.7,
                    0.3
                ],
                "train_verbose": 1,
                "training_id": 49,
                "updated_on": "2020-01-01T00:00:00Z",
                "weight_dir": "./data/weights"
            }
        }


AI Agent update
+++++++++++++++++

.. http:patch:: /v1/ai-agent/(integer:ai_agent)

    Update a version.

    **Example request**:

    .. tabs::

        .. code-tab:: bash

            $ curl \
              -X PATCH \
              -H "Authorization: Token <token>" http://macula-ai/v1/ai-agent/(integer:ai_agent) \
              -H "Content-Type: application/json" \
              -d '"training_id": 38'

        .. code-tab:: python

            import requests
            import json
            URL = 'http://macula-ai/v1/ai-agent/(integer:ai_agent)'
            TOKEN = '<token>'
            HEADERS = {'Authorization': f'token {TOKEN}'}
            data = json.load(open('body.json', 'rb'))
            response = requests.patch(
                URL,
                json=data,
                headers=HEADERS,
            )
            print(response.json())

    The content of ``body.json`` is like,

    .. sourcecode:: json

        {
            "data": {
                "agent_id": 49,
                "batch_size": 64,
                "callbacks": [
                    "weightcheckpoint",
                    " confusionmatrix",
                    " jsonlogger"
                ],
                "camera_id": null,
                "data_json": "./data/dataset/Semiconductor_v0.0.0_128X128_1000_images.json",
                "description": null,
                "epoch": 10,
                "gpu": [
                    -1
                ],
                "images_data_dir": "./data/dataset/image",
                "img_ext": "jpg",
                "inference_path": null,
                "init_epoch": 0,
                "input_shape": [
                    128,
                    128,
                    3
                ],
                "last_layer_activation": "sigmoid",
                "learning_rate": 0.0001,
                "log_dir": "./data/logs",
                "loss_list": [
                    "bce"
                ],
                "metrics": [
                    "accuracy ",
                    "precision",
                    " recall",
                    " confusionmatrix"
                ],
                "model_id": 1,
                "model_path": "./data/models",
                "model_type": "classification",
                "normalize_confusion_matrix": false,
                "optimizer_id": "Adam",
                "pretrained": true,
                "random_brightness": false,
                "random_crop": true,
                "random_flip": true,
                "random_rotate": true,
                "runtime": 1577854800.0,
                "save_best_model": "true",
                "split_index": [
                    0.7,
                    0.3
                ],
                "train_verbose": 1,
                "training_id": 38,
                "updated_on": "2020-01-01T00:00:00Z",
                "weight_dir": "./data/weights"
            }
        }

    :statuscode 204: Updated successfully


AI Agent delete
+++++++++++++++++

.. http:delete:: /v1/ai-agent/(integer:ai_agent)

    Delete a subproject relationship.

    **Example request**:

    .. tabs::

        .. code-tab:: bash

            $ curl \
              -X DELETE \
              -H "Authorization: Token <token>" http://macula-ai/v1/ai-agent/(integer:ai_agent)

        .. code-tab:: python

            import requests
            URL = 'http://macula-ai/v1/ai-agent/(integer:ai_agent)'
            TOKEN = '<token>'
            HEADERS = {'Authorization': f'token {TOKEN}'}
            response = requests.delete(URL, headers=HEADERS)
            print(response.json())

    :statuscode 204: Subproject deleted successfully

Inference instantiation
++++++++++++++++++++++++

.. http:get:: /v1/inference/start/(integer:ai_agent)

    Retrieve details of a single project.

    **Example request**:

    .. tabs::

        .. code-tab:: bash

              $ curl \
              -X GET \
              -H "Authorization: Token <token>" http://macula-ai.com/v1/inference/start/(integer:ai_agent) \
              -H "Content-Type: application/json"

        .. code-tab:: python

            import requests
            URL = 'http://macula-ai.com/v1/inference/start/(integer:ai_agent)'
            TOKEN = '<token>'
            HEADERS = {'Authorization': f'token {TOKEN}'}
            response = requests.get(URL, headers=HEADERS)
            print(response.json())

    **Example response**:

    .. sourcecode:: json

        {
          "agent_id": 1,
          "attributes": "[{\"id\": 0, \"name\": \"Good\", \"confidence\": 0.0, \"label\": 0}, {\"id\": 1, \"name\": \"Broken_Leg\", \"confidence\": 1.0, \"label\": 1}]",
          "categories": [],
          "confidence": 1.0,
          "file_name": "Broken_Leg_754.JPG",
          "file_path": "./data/macula/agents/1/infer_images/Broken_Leg_754.JPG",
          "good": false,
          "image_b64": "/9j/4AAQSkZJRgABAQAAAQABAAD/2wBDAAgGBgcGBQgHBwcJCQgKDBQNDAsLDBkSEw8UHRofHh0aHBwgJC4nICIsIxwcKDcpLDAxNDQ0Hyc5PTgyPC4zNDL/wAALCAlgAAMBAREA/8QAHwAAAQUBAQEBAQEAAAAAAAAAAAECAwQFBgcICQoL/8QAtRAAAgEDAwIEAwUFBAQAAAF9AQIDAAQRBRIhMUEGE1FhByJxFDKBkaEII0KxwRVS0fAkM2JyggkKFhcYGRolJicoKSo0NTY3ODk6Q0RFRkdISUpTVFVWV1hZWmNkZWZnaGlqc3R1dnd4eXqDhIWGh4iJipKTlJWWl5iZmqKjpKWmp6ipqrKztLW2t7i5usLDxMXGx8jJytLT1NXW19jZ2uHi4+Tl5ufo6erx8vP09fb3+Pn6/9oACAEBAAA/APAD1obr+AoPX8BQ3X8BQw5/AUN1/AfyqxIQHxjsKhkIL59hUpQjA9h/KnTy4lI9h/KoJDl8+w/lTW6/gP5Ur/e/AfypzoQ5prk7vwFI3X8B/KlYc/gKcy9PoP5Usijf+A/lTZDl8+w/lTW6/gP5UN1/Afyob71K4+c0MefwH8qRuv4D+VSnPH0FPI6fQVExOR9B/KmsefwH8qVjz+AoYc/gP5U4g8fQfyp5I4+gqJjyPoP5UMpyPoP5UjnLfgP5VL5QIB9hUyjCKPYVPIqqwH+yP5VVnmbzjz6fyqKXd5h/D+VOc5bPsKd5ZIBx2FPL4xz2H8qqufm/Afyof734D+VS+Vn8qeccfQfypzAZ/AUwg5pSwzSHIP4U2SQiQimSgmQmpNmQD7CpyyjA9hVORjv69h/KpHh+b8B/Kns3I+g/lTcHA57CpdpwPoKTzen0FQsAG/AU5lO4809gMj6D+VQPnd97sP5U4gZH0FTmNRgew/lTZZcSHg9B/KkkC7zSSD5zTJWHmH6D+VPZeaeyc0M3PSl8vgcdhUhPTjsP5VDO+JiPYfyqCZy0pOfT+VI4G6pXT5unYfyp820yE+w/lTthIBx2H8quMEDYx2FZ0s7eYcHsP5VDJ981LJF85omkzKT9P5VEck5zSN940N1/AVIx5/AUx/vfgP5UEc/gKCefwFDjDUOfm/AfyqQYAH0FNY4I+g/lTX+9+A/lSN978BSt1/AUH+lKw5/AfypHPzfgKGPP4CpGi5/AU+U/vD+FQuSWzikYkH8B/Kny7fMP4UkmfMNSyBPMPSoZCd5PqAafKiiQjPQAfpTJjmZjSMcnNDjDmlkPzmmt1/AUsn3/AMB/Kh/vmh/vmlkHz/gP5VKyLnp2FLKv7w/hTZT+9NMmwZTj2/lTXHzmhx8xpWU5/Afyp8kgL/gP5UMnPTsKJ2PnN+H8qZJkuTjsP5UrE569hTW6/gKWT7/4D+VEjHf17D+VI/LfgP5UOMN+A/lT5CPMNNkI3mkY/NSsOaRx834D+VD/AHvwFOc4anSqBIaZL/rT+FK6sX/AUsn3+nYfyp8zr5p/D+VRyE7zx2H8qJ8eafoP5UjsS2T6D+VEgw/4D+VEn3/wH8qkmVfOb602cgzN9abIcyGkbr+AoYc/gKWQfP8AgP5U1jk59hQwOelK4w35U6VcSH8KfLnzTTZWPmmnyqPMP4UjOc01t26pHYhyM0yY4mao3+9+AocYb8B/KnOp3fgP5VLIx3moJPvnNK+N3TsKJFO88VO5i3feqCQDf+A/lSSff/Afyp8gG/8AAVPIo39OwpXch/wFJNjzWqKVv3h/CiVR5h/Clyf0p5UZpkyDzT+H8qZIXLk1I2M/gKZI3z/gP5U2Q/OadIv7w1NIpEjCi4mQXD/WoJFPmGnyH94aJR+8apJEO80SOvmGkkQeYeaSVv3hqORvnNKTT9owPl7VYUKFAJ7CoJGIciiVR5hqR4xuPNNkc7+vYfypJEbf+A/lTpPv/gP5U1gc/gP5UkhG802QHzDUjsu81G6/OaSU/vD+H8qSYHzTz6fyp8gPmGkkPz/gKcQRgew/lSStiQ/h/KlkA3nnsP5UTIBKefSpbnYbhiP88UjjLZ9hSOfm/AfyqOQ/vDUrDn8KVl5/AUr5D4+lJPkTNVhiufwprDn8BUUxHmmkkB3miX/WGlnP75vw/lSy4800+U/vTUU5/fN+H8qfOn75vw/lRMzeaaZKP3h/D+VOkB3mnSRjeaWUnzDTmUbjSSL8/wCVEitv/AVM7JvNOeNN5p0pAkPNDDcc+oH8qrvMd34D+VLJbHefwqaSc7zTHA3fgP5Uxs579BV1lTNIsgCjmq5Vgfwq4yDdUDTNnv0FRlun0H8qjbOelXET5F+YdKcEO0degqBpefwFVmBz07CrMkPzninSTDeaRkyc08fdH0FOA+VfoKjDHaPoKaUxj6CkkbDkew/lT2xmmHt9BSSBt/XsP5VYe2bd17D+VX9wAHPYU18huvYfypvmggHHYVCmTGv0FSKnyL83YUBWwOe1X106SRFfb1Aq/BfIsEa4XhRXN/aGwOew/lUYTgfQV//Z",
          "inference_id": 6,
          "result": "NG",
          "updated_on": "2020-10-20T13:30:43Z"
        }

    :query string expand: allows to add/expand some extra fields in the response.
                          Allowed values are ``active_versions``, ``active_versions.last_build`` and
                          ``active_versions.last_build.config``. Multiple fields can be passed separated by commas.



Inference stopping
+++++++++++++++++++

.. http:post:: /v1/inference/stop/(integer:ai_agent)

    Retrieve details of a single project.

    **Example request**:

    .. tabs::

        .. code-tab:: bash

            $ curl -H "Authorization: Token <token>" http://macula-ai.com/v1/inference/stop/(integer:ai_agent)

        .. code-tab:: python

            import requests
            URL = 'http://macula-ai.com/v1/inference/stop/(integer:ai_agent)'
            TOKEN = '<token>'
            HEADERS = {'Authorization': f'token {TOKEN}'}
            response = requests.get(URL, headers=HEADERS)
            print(response.json())

    **Example response**:

    .. sourcecode:: json

        {
          "agent_id": 1,
          "attributes": [
                            {
                              "id": 0,
                              "name": "Good",
                              "confidence": 0.0,
                              "label": 0
                            },
                            {
                              "id": 1,
                              "name": "Broken_Leg",
                              "confidence": 1.0,
                              "label": 1
                            }
                         ],
          "categories": [],
          "confidence": 1.0,
          "file_name": "Broken_Leg_754.JPG",
          "file_path": "./data/macula/agents/1/infer_images/Broken_Leg_754.JPG",
          "good": false,
          "image_b64": "/9j/4AAQSkZJRgABAQAAAQABAAD/2wBDAAgGBgcGBQgHBwcJCQgKDBQNDAsLDBkSEw8UHRofHh0aHBwgJC4nICIsIxwcKDcpLDAxNDQ0Hyc5PTgyPC4zNDL/wAALCAlgAAMBAREA/8QAHwAAAQUBAQEBAQEAAAAAAAAAAAECAwQFBgcICQoL/8QAtRAAAgEDAwIEAwUFBAQAAAF9AQIDAAQRBRIhMUEGE1FhByJxFDKBkaEII0KxwRVS0fAkM2JyggkKFhcYGRolJicoKSo0NTY3ODk6Q0RFRkdISUpTVFVWV1hZWmNkZWZnaGlqc3R1dnd4eXqDhIWGh4iJipKTlJWWl5iZmqKjpKWmp6ipqrKztLW2t7i5usLDxMXGx8jJytLT1NXW19jZ2uHi4+Tl5ufo6erx8vP09fb3+Pn6/9oACAEBAAA/APAD1obr+AoPX8BQ3X8BQw5/AUN1/AfyqxIQHxjsKhkIL59hUpQjA9h/KnTy4lI9h/KoJDl8+w/lTW6/gP5Ur/e/AfypzoQ5prk7vwFI3X8B/KlYc/gKcy9PoP5Usijf+A/lTZDl8+w/lTW6/gP5UN1/Afyob71K4+c0MefwH8qRuv4D+VSnPH0FPI6fQVExOR9B/KmsefwH8qVjz+AoYc/gP5U4g8fQfyp5I4+gqJjyPoP5UMpyPoP5UjnLfgP5VL5QIB9hUyjCKPYVPIqqwH+yP5VVnmbzjz6fyqKXd5h/D+VOc5bPsKd5ZIBx2FPL4xz2H8qqufm/Afyof734D+VS+Vn8qeccfQfypzAZ/AUwg5pSwzSHIP4U2SQiQimSgmQmpNmQD7CpyyjA9hVORjv69h/KpHh+b8B/Kns3I+g/lTcHA57CpdpwPoKTzen0FQsAG/AU5lO4809gMj6D+VQPnd97sP5U4gZH0FTmNRgew/lTZZcSHg9B/KkkC7zSSD5zTJWHmH6D+VPZeaeyc0M3PSl8vgcdhUhPTjsP5VDO+JiPYfyqCZy0pOfT+VI4G6pXT5unYfyp820yE+w/lTthIBx2H8quMEDYx2FZ0s7eYcHsP5VDJ981LJF85omkzKT9P5VEck5zSN940N1/AVIx5/AUx/vfgP5UEc/gKCefwFDjDUOfm/AfyqQYAH0FNY4I+g/lTX+9+A/lSN978BSt1/AUH+lKw5/AfypHPzfgKGPP4CpGi5/AU+U/vD+FQuSWzikYkH8B/Kny7fMP4UkmfMNSyBPMPSoZCd5PqAafKiiQjPQAfpTJjmZjSMcnNDjDmlkPzmmt1/AUsn3/AMB/Kh/vmh/vmlkHz/gP5VKyLnp2FLKv7w/hTZT+9NMmwZTj2/lTXHzmhx8xpWU5/Afyp8kgL/gP5UMnPTsKJ2PnN+H8qZJkuTjsP5UrE569hTW6/gKWT7/4D+VEjHf17D+VI/LfgP5UOMN+A/lT5CPMNNkI3mkY/NSsOaRx834D+VD/AHvwFOc4anSqBIaZL/rT+FK6sX/AUsn3+nYfyp8zr5p/D+VRyE7zx2H8qJ8eafoP5UjsS2T6D+VEgw/4D+VEn3/wH8qkmVfOb602cgzN9abIcyGkbr+AoYc/gKWQfP8AgP5U1jk59hQwOelK4w35U6VcSH8KfLnzTTZWPmmnyqPMP4UjOc01t26pHYhyM0yY4mao3+9+AocYb8B/KnOp3fgP5VLIx3moJPvnNK+N3TsKJFO88VO5i3feqCQDf+A/lSSff/Afyp8gG/8AAVPIo39OwpXch/wFJNjzWqKVv3h/CiVR5h/Clyf0p5UZpkyDzT+H8qZIXLk1I2M/gKZI3z/gP5U2Q/OadIv7w1NIpEjCi4mQXD/WoJFPmGnyH94aJR+8apJEO80SOvmGkkQeYeaSVv3hqORvnNKTT9owPl7VYUKFAJ7CoJGIciiVR5hqR4xuPNNkc7+vYfypJEbf+A/lTpPv/gP5U1gc/gP5UkhG802QHzDUjsu81G6/OaSU/vD+H8qSYHzTz6fyp8gPmGkkPz/gKcQRgew/lSStiQ/h/KlkA3nnsP5UTIBKefSpbnYbhiP88UjjLZ9hSOfm/AfyqOQ/vDUrDn8KVl5/AUr5D4+lJPkTNVhiufwprDn8BUUxHmmkkB3miX/WGlnP75vw/lSy4800+U/vTUU5/fN+H8qfOn75vw/lRMzeaaZKP3h/D+VOkB3mnSRjeaWUnzDTmUbjSSL8/wCVEitv/AVM7JvNOeNN5p0pAkPNDDcc+oH8qrvMd34D+VLJbHefwqaSc7zTHA3fgP5Uxs579BV1lTNIsgCjmq5Vgfwq4yDdUDTNnv0FRlun0H8qjbOelXET5F+YdKcEO0degqBpefwFVmBz07CrMkPzninSTDeaRkyc08fdH0FOA+VfoKjDHaPoKaUxj6CkkbDkew/lT2xmmHt9BSSBt/XsP5VYe2bd17D+VX9wAHPYU18huvYfypvmggHHYVCmTGv0FSKnyL83YUBWwOe1X106SRFfb1Aq/BfIsEa4XhRXN/aGwOew/lUYTgfQV//Z",
          "inference_id": 6,
          "result": "NG",
          "updated_on": "2020-10-20T13:30:43Z"
        }



