User guide
============================================

In this user guide, we will use curl to demonstrate the step to get inference results.

1 . Login
----------------------------------

Login via the authentification service


    .. tabs::

        .. code-tab:: bash

            $ curl \
              -X POST http://macula-ai.com/v1/auth/login
              -H "Content-Type: application/json"
              -d  '{"username":"macula-admin","password":"macula123!"}'

        .. code-tab:: python

            import requests
            URL = 'http://macula-ai.com/v1/auth/login'
            data = json.load(open('body.json', 'rb'))
            response = requests.post(URL, json=data)
            print(response.json())

2 . AI Agent selection
---------------------------

Select your AI Agent

    **Example request**:

    .. tabs::

        .. code-tab:: bash

            $ curl -H "Authorization: Token <token>" http://macula-ai/v1/inference/summary/(integer:ai_agent)

        .. code-tab:: python

            import requests
            URL = 'http://macula-ai/v1/ai-agent/(integer:ai_agent)'
            TOKEN = '<token>'
            HEADERS = {'Authorization': f'token {TOKEN}'}
            response = requests.get(URL, headers=HEADERS)
            print(response.json())



3 . Inference result
---------------------------

Once you have the selection done, you select the given AI Agent

    .. tabs::

        .. code-tab:: bash

              $ curl \
              -X GET \
              -H "Authorization: Token <token>" http://macula-ai.com/v1/inference/start/(integer:ai_agent) \
              -H "Content-Type: application/json"

        .. code-tab:: python

            import requests
            URL = 'http://macula-ai.com/v1/inference/start/(integer:ai_agent)'
            TOKEN = '<token>'
            HEADERS = {'Authorization': f'token {TOKEN}'}
            response = requests.get(URL, headers=HEADERS)
            print(response.json())