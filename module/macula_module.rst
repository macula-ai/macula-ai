Solution description
==============================

Macula Solution is a highly customizable and scalable solution for computer vision projects. Images from
production assembly lines are used as an input to the system to classify objects either as defected or not-defected.
Quick and early detection of defects can greatly increase production efficiency and product quality, as well as prevent
extra costs by preventing passing defective parts downstream in the assembly and shipping process.

Macula can be deployed on-premises, cloud, or on the edge. The core of Macula Solution is the Computer Vision Backend that
interacts with storage and NVIDIA TensorRT Inference Server  (NTRT-IS) which serves the model to be used for inspection.
See diagram 1 below for a high-level architecture of DIS.

.. image:: ../img/high-level-architecture.png

In this current architecture, the concept of AI Agent was necessary to controle the different deployment into production.

.. image:: ../img/ai-agent.png